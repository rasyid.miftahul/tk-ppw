from django.urls import include, path
from . import views

app_name = 'semangatpage'

urlpatterns = [
    path('', views.semangat, name='semangat'),
    path('addsemangat/', views.add_semangat, name='addsemangat'),
]