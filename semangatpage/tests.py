from django.test import TestCase, Client
from django.urls import resolve
from .views import semangat, add_semangat
from .models import kata
from .forms import kataForm


class TestSemangatPage(TestCase):

#urlTest
    def test_url_semangat(self): #GET
        response = Client().get('/semangatpage/')
        self.assertEqual(response.status_code, 200)

    def test_url_addsemangat(self): #GET
        response = Client().get('/semangatpage/addsemangat/')
        self.assertEqual(response.status_code, 200)
    
    def test_tambah_kata_semangat(self): #POST
        response = Client().post('/semangatpage/addsemangat/', {'katasemangat': 'semangat!',})
        jumlah = kata.objects.filter(katasemangat = 'semangat!').count()
        self.assertEqual(jumlah, 1)


#viewsTest
    def test_template_semangat_is_used(self): #test template used
        response = Client().get('/semangatpage/')
        self.assertTemplateUsed(response, 'semangatpage/semangat.html')

    def test_template_add_semangat_is_used(self): #test template used
        response = Client().get('/semangatpage/addsemangat/')
        self.assertTemplateUsed(response, 'semangatpage/addsemangat.html')

    def test_views_semangat(self): #test function
        found = resolve('/semangatpage/')
        self.assertEqual(found.func, semangat)

    def test_views_add_semangat(self): #test function
        found = resolve('/semangatpage/addsemangat/')
        self.assertEqual(found.func, add_semangat)

#modelTest
    def test_cek_model_create_kata(self): #functionality test
        kata.objects.create(katasemangat="semangat")
        jumlah = kata.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_cek_model_return_kata(self): #return test
        kata.objects.create(katasemangat="semangat")
        hasil = kata.objects.get(id=1)
        self.assertEqual(str(hasil), "semangat")

#formTest
    def test_form_semangat_is_valid(self): #form validation test
        form = kataForm(data={'katasemangat':'semangat'})
        self.assertTrue(form.is_valid())

    def test_form_semangat_is_invalid(self): #form invalidation test
        form = kataForm(data={})
        self.assertFalse(form.is_valid())



    