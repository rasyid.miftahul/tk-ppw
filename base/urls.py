from django.urls import path
from . import views

app_name = 'base'

urlpatterns = [
    path('', views.index, name='base'),
    path('/masker', views.masker, name='masker'),
    path('/sanitizer', views.sanitizer, name='sanitizer'),
    path('/faceshield', views.faceshield, name='faceshield'),


]