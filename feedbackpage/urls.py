from django.urls import path
from . import views

app_name = 'feedbackpage'
urlpatterns = [
    path('', views.feedback, name='feedback'),
    path('saveFeedback/', views.saveFeedback, name='saveFeedback'),
]