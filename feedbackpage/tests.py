from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import feedback, saveFeedback
from .models import Feedback
from .forms import Input_Form


# Create your tests here.
class FeedTest(TestCase):
    def test_feedbackpage_url(self):
        response = Client().get('/feedbackpage/')
        self.assertEqual(response.status_code, 200)

    def test_using_feedbackpage_template(self):
        response = Client().get('/feedbackpage/')
        self.assertTemplateUsed(response, 'feedbackpage/feedbackpage.html')

    def test_using_feedback_function(self):
        found = resolve('/feedbackpage/')
        self.assertEqual(found.func, feedback)

    def test_using_model_feedback(self):
        Feedback.objects.create(content='a')
        data = Feedback.objects.all().count()
        self.assertEqual(data, 1)

    def test_form_feedback_valid(self):
        form = Input_Form(data={'content':'PPW'})
        self.assertTrue(form.is_valid())

    def test_form_feedback_invalid(self):
        form= Input_Form(data={})
        self.assertFalse(form.is_valid())

    def test_using_action_page(self):
        found = resolve('/feedbackpage/saveFeedback/')
        self.assertEqual(found.func, saveFeedback)

    def test_post_form_feedback_valid(self):
        arg = {'content' : 'test'}
        response = Client().post('/feedbackpage/saveFeedback/', arg)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'feedbackpage/saveFeedback.html')
        data = Feedback.objects.all().count()
        self.assertEqual(data, 1)

    def test_post_form_feedback_invalid(self):
        arg = {'content' : ''}
        response = Client().post('/feedbackpage/saveFeedback/', arg)
        self.assertTemplateUsed(response, 'feedbackpage/feedbackpage.html')
        data = Feedback.objects.all().count()
        self.assertNotEqual(data, 1)


