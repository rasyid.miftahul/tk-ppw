from django.urls import path
from . import views

app_name = 'jadwal'
# app_name = 'postingan'
urlpatterns = [
    path('', views.jadwal, name='jadwal'),
    path('edit/', views.edit, name='edit'),
]