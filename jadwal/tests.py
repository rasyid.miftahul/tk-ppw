from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import jadwal, edit
from .models import Jadwal, Kriteria
from .forms import ListJadwal
# Create your tests here.

class UnitTestJadwal (TestCase):
#urltest
    def test_url_jadwal (self):
        response = Client().get('/jadwal/')
        self.assertEquals (response.status_code,200)
    

    def test_url_edit (self):
        response = Client().get('/jadwal/edit/')
        self.assertEquals (response.status_code,200)

#viewtest
    def test_template_jadwal_used(self):
        response = Client().get('/jadwal/')
        self.assertTemplateUsed(response, 'tambahjadwal.html')

    def test_template_edit_used(self):
        response = Client().get('/jadwal/edit/')
        self.assertTemplateUsed(response, 'tambahjadwal.html')

    def test_viewfunc_jadwal (self):
        found = resolve('/jadwal/')
        self.assertEqual(found.func, jadwal)
    
    def test_viewfunc_edit (self):
        found = resolve('/jadwal/edit/')
        self.assertEqual(found.func, edit)

#modeltest
    def test_model_Kriteria (self):
        testkriteria = Kriteria.objects.create(
            kriteria = 'testing'
        )
        self.assertTrue(isinstance(testkriteria,Kriteria))
        self.assertEqual('testing', str(testkriteria))
        self.assertTrue(Client().get('/jadwal/'))

    def test_model_jadwal (self):
        testjadwal = Jadwal.objects.create(
            jadwal = 'testing'
        )
        testjadwal.save()
        self.assertTrue(isinstance(testjadwal,Jadwal))
        # self.assertTrue(Client().get('/jadwal/'))

    def test_form_jadwal_kosong (self):
        form = ListJadwal (
            data = {'jadwal':'','kriteria':''}
        )
        self.assertFalse(form.is_valid())
    
    def test_form_jadwal_accepted(self):
        kriteria = Kriteria.objects.create (
            kriteria = 'testing'
        )
        form = ListJadwal (
            data = {'jadwal':'test','kriteria':kriteria}
        )
        self.assertTrue(form.is_valid())
    
    # def test_view_redirect_POST (self):
    #     response = self.client.post (
    #         '/jadwal/edit/', {'jadwal':'test'}
    #     )