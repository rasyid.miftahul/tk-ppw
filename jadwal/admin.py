from django.contrib import admin

# Register your models here.
from .models import Jadwal, Kriteria
admin.site.register(Jadwal)
admin.site.register(Kriteria)
