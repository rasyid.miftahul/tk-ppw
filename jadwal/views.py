from django.shortcuts import render, redirect
from .forms import ListJadwal
from .models import Jadwal, Kriteria
# Create your views here.

def jadwal (request):
    submit = False
    list_jadwal = Jadwal.objects.all()
    kriteria = Kriteria.objects.all()

    if request.method == 'POST':
        form = ListJadwal(request.POST)
        if form.is_valid():
            form.save()
        return redirect ('/jadwal/')

    else:
        form = ListJadwal()
        if 'submit' in request.GET:
            submit = True
    return render(request, 'tambahjadwal.html', {
        'form': form, 'submit': submit, 
        'kriteria': kriteria,
        'list_jadwal': list_jadwal,
        }
    )

def edit (request):
    if request.method == "POST":
        Jadwal.objects.get(id=request.POST['id']).delete()
        return redirect('/jadwal/')
    list_jadwal = Jadwal.objects.all()
    return render(request, 'tambahjadwal.html', {'list_jadwal': list_jadwal})



