ANGGOTA KELOMPOK

1. Falih Mafazan
2. Nadhira Rafik
3. Rajendra Daniel Saksono
4. Rasyid Miftahul Ihsan
5. Syifa Rizka Sagita

TENTANG APLIKASI INI

Aplikasi ini merupakan sebuah aplikasi yang bertujuan untuk membantu petugas kesehatan dan masyarakat dalam menghadapi pandemi COVID-19. Saat ini ada 5 fitur yang dapat diakses oleh user yaitu :


1. Atur jadwal
Fitur ini sangat berguna karena di masa pandemi masyarakat terpaksa mengatur ulang jadwal sehari-hari mereka. Fitur ini
akan dibagi menjadi dua yaitu "Critical" dan "Non-critical".

2. Berbagi semangat
Fitur ini bertujuan untuk saling mendukung satu sama lain antar individu agar tetap semangat dalam menjalani hari di masa
pandemi.

3. Membuat post informatif 
Fitur ini dapat digunakan user untuk menulis sebuah postingan (dapat berisi foto) yang informatif terkait covid-19.

4. Pooling barang kebutuhan pandemi
Fitur ini bertujuan untuk mendapatkan data barang-barang apa saja yang paling banyak digunakan orang saat pandemi.

5. Feedback
Fitur ini bertujuan agar kami tim developer dapat mengembangkan aplikasi ini menjadi lebih laik lagi dengan memanfaatkan
data ang diinput oleh user.
                      

- Link Herokuapp : https://pandenemy.herokuapp.com/
- Link Wireframe : https://www.figma.com/file/3UlWLyByxVknTHdfnwBI2W/TK-PPW-1?node-id=20%3A2
- Link Mockup    : https://www.figma.com/file/3UlWLyByxVknTHdfnwBI2W/TK-PPW-1?node-id=0%3A1
- Link Persona   : https://docs.google.com/document/d/1HE6lqgBpxjeVmc_22DoEw8e8ddTkj_kmCyUiJMJAnks/edit?usp=sharing


PIPELINE

![pipeline status](https://gitlab.com/rasyid.miftahul/tk-ppw/badges/master/pipeline.svg) ![coverage report](https://gitlab.com/rasyid.miftahul/tk-ppw/badges/master/coverage.svg)