from django.urls import path
from . import views


# app_name = 'postingan'
urlpatterns = [
    path('postingan/', views.index, name='index'),
    path('postingan/upload/', views.upload, name='uploadGambar'),
    path('postingan/detail/<int:input_id>', views.detail, name='detail')
]