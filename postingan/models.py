from django.db import models
from PIL import Image

# Create your models here.

class Gambar(models.Model):
    file_gambar = models.ImageField(upload_to='media/postingan')
    judul = models.CharField(max_length=50)
    deskripsi = models.TextField(max_length=500)

    def __str__(self):
        return self.judul

    # def save(self):
        

    #     img = Image.open(self.file_gambar.path)

    #     output_size = (300,300)
    #     img.thumbnail(output_size)
    #     img.save(self.file_gambar.path)

    #     super().save()

